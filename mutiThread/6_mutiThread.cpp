//
// Created by 敬搏涛 on 2022/3/7.
//
#include <iostream>
#include <thread>
#include <chrono>
void joinWorker() {

}

class Obj {
public:
    Obj() {
        std::cout << "hello";
    }
    ~Obj() {
        std::cout << " world\n";
    }
};

void detachWorker() {
    Obj obj;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

class ThreadGuard {
public:
    ThreadGuard(std::thread& t): m_thread(t) {}
    ~ThreadGuard() {
        if(m_thread.joinable()) {
            m_thread.join();
        }
    }
private:
    std::thread& m_thread;
};


int main() {
    std::thread j(joinWorker);
    std::thread w(detachWorker);
    //w.detach(); // 只有在线程比主进程时间短很多时调用,否则可能导致线程提前结束而未执行完成
    if(j.joinable()) {
        j.join();
    }
    if(w.joinable()) {
        w.join();
    }
    std::thread h(detachWorker);
    ThreadGuard tg(h);
    return 0;
}