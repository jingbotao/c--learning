//
// Created by 敬搏涛 on 2022/3/9.
//
#include "iostream"
#include <list>
#include <chrono>
#include "thread"
// 多线程案例 问题提出
using namespace std;

class Message {
public:
    const string& data() const {
        return m_data;
    }
    Message(string d): m_data(move(d)) {}
private:
    string m_data;
};
std::list<Message> globalList;
int totalSize = 0;
void worker() {
    while(!globalList.empty()) {
        auto iter = globalList.begin();
        //cout << "do something!" << endl;
        totalSize += strlen((*iter).data().c_str());
        globalList.erase(iter);
    }
}

int main() {
    auto nowc = clock();
    for(int i = 0; i < 10000; ++i) {
        globalList.push_back("this is a test" + to_string(i));
    }
    worker();
     for(int i = 0; i < 10000; ++i) {
         std::this_thread::sleep_for(chrono::milliseconds(1));
         globalList.push_back(string("second"));
     }
     worker();
    cout << "bye bye" << endl;
    auto finish = clock();
    cout << "总耗时：" << finish - nowc << endl;
    cout << "总数据大小：" << totalSize << endl;
    return 1;
}