//
// Created by 敬搏涛 on 2022/3/6.
//
// 多线程-引用传值
#include <iostream>
#include <thread>
using namespace std;

void printAll(int a, int b, int c) {
    cout << a << ' ' << b << ' ' << c << endl;
}

void add(int a, int b, int& c) {
    c += a;
    c += b;
}

void printString(const string& info, const string& info2) {
    cout << info << info2 << endl;
}

int main() {
    int a = 3, b = 4, c = 5;
    thread t([=] {
        printAll(a, b, c);
    });
    t.join();
    thread t2(printAll, a, b, c);
    t2.join();

    thread t3([=, &c] {
        add(a, b, c); // 推荐
    });
    t3.join();
    cout << "after add: " << c << endl;
    thread t4(add, a, b, ref(c));
    t4.join();
    cout << "after add: " << c << endl;

    std::string abc("abc");
    std::string def("def");
    thread t5([&] {
        printString(abc, def); // 推荐
    });
    t5.join();
    thread t6(printString, abc, def);
    t6.join();
    thread t7(printString, cref(abc), cref(def));
    t7.join();
    return 0;
}

