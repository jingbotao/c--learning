#include <iostream>
#include <thread>
#include <future>
#include <cmath>
#include <vector>
#include <chrono>
#include <cstdlib>

/*30:0:0*/

double caculate(double v) {
    if (v <= 0) return v;
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    return sqrt((v * v + std::sqrt((v - 5) * (v + 2.5)) / 2.0) / v);
}

template <typename Iter, typename Fun>
double visitRange(std::thread::id id, Iter iterBegin, Iter iterEnd, Fun func) {
    auto curId = std::this_thread::get_id(); // 获取当前线程的id
    if(id == curId) { // 当前线程的ID与主线程ID比较
        std::cout << curId << " hello main thread\n";
    } else {
        std::cout << curId << " hello work thread\n";
    }
    double v = 0;
    for(auto iter = iterBegin; iter != iterEnd; ++iter) {
        v += func(*iter);
    }
    return v;
}



int main() {
    auto mainThreadId = std::this_thread::get_id();
    std::vector<double> v; // 创建待计算的数组
    for(int i = 0; i < 1000; ++i) {
        v.push_back(rand());
    }
    std::cout << v.size() << std::endl;
    double value = 0.0;
    auto nowc = clock(); // 计时器
    for(auto& info: v) {
        value += caculate(info);
    }
    auto finishc = clock();
    std::cout << "single thread: " << value << " used" << (finishc - nowc) << std::endl;

    nowc = clock();
    auto iter = v.begin() + (v.size() / 2);
    double anotherv = 0.0;
    auto iterEnd = v.end();
    std::thread s([&anotherv, mainThreadId, iter, iterEnd]() {
        anotherv = visitRange(mainThreadId, iter, iterEnd, caculate);
    });
    auto halfv = visitRange(mainThreadId, v.begin(), iter, caculate);
    s.join(); // 等待子线程执行完成
    finishc = clock();
    std::cout << "multithead: " << (halfv + anotherv) << " used" << (finishc - nowc) << std::endl;
}
// 由于多线程的切换有较大的开销，故在执行较小的任务的时候使用多线程可能并不会减少执行时间。
