//
// Created by 敬搏涛 on 2022/3/5.
//
#include <iostream>
#include <thread>
#include <future>
#include <cmath>
#include <vector>
#include <chrono>
#include <cstdlib>
using namespace  std;
// 多线程变量需要对共享变量进行处理
// 处理：
// 1. 取消变量共享，单独计算，然后汇总（如果没有必要的话，线程间不要共享资源）
class Counter {
public:
    void addCount() {
        m_count++;
    }
    int count() const {
        return m_count;
    }
    Counter(): m_count{0} {}
private:
    int m_count;
};

int work(int a) {
    return a + a;
}

template <class Iter>
void realWork(Counter &c, double &totalValue, Iter b, Iter e) {
    for(;b != e; ++b) {
        totalValue += work(*b);
        c.addCount();
    }
}


int main() {
    int n = std::thread::hardware_concurrency();
    std::cout << n << "个线程能够支持！" << std::endl;
    vector<int> vec;
    double totalValue = 0;
    for(int i = 0; i < 10000000; ++i) {
        vec.push_back(rand() % 100);
    }
    Counter counter;
    realWork(counter, totalValue, vec.begin(), vec.end());
    std::cout << "total time: " << counter.count() << " " << totalValue << endl;
    totalValue = 0;
    Counter counter2;
    auto iter = vec.begin() + (vec.size() / 3);
    auto iter2 = vec.begin() + (vec.size() / 3 * 2);
    thread b([&counter2, &totalValue, iter, iter2] {
       realWork(counter2, totalValue, iter, iter2);
    });
    auto end = vec.end();
    thread c([&counter2, &totalValue, iter2, end] {
       realWork(counter2, totalValue, iter2, end);
    });
    realWork(counter2, totalValue, vec.begin(), iter);
    b.join();
    c.join();
    cout << "total times use multithread: " << counter2.count() << " " << totalValue << endl;
}
