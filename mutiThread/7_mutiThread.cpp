//
// Created by 敬搏涛 on 2022/3/7.
//
#include "thread"
#include <mutex>
#include "atomic"
#include "vector"
#include "iostream"
#include "chrono"

using namespace std;

std::mutex m_mutex;
std::atomic<bool> ready{false};

void worker(int i) {
    while(!ready) {
        // do something;
        //std::this_thread::yield(); // 释放当前资源，其他线程可抢占,可能不会释放资源
        std::this_thread::sleep_for(std::chrono::seconds(1)); //
    }
    lock_guard<std::mutex> lock(m_mutex);
    cout << "hello world " << i << endl;
}

int main() {
    const auto threadCount = 4;
    vector<thread> pool;
    for(int i = 0; i < threadCount; ++i) {
        pool.emplace_back(worker, i);
    }
    std::this_thread::sleep_for(std::chrono::seconds (10 ));
    ready = true;
    for(auto & v: pool) {
        if(v.joinable()) {
            v.join();
        }
    }
    cout << "bye bye" << endl;
    return 1;
}