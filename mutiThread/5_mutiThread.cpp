//
// Created by 敬搏涛 on 2022/3/7.
//
#include <iostream>
#include "mutex"
using namespace std;

template <typename T>
class Lock{
    // 自定义锁，实现自动释放
public:
    Lock(T& mutex): m_mutex(mutex) {
        m_mutex.lock();
    }
    ~Lock() {
        m_mutex.unlock();
    }
private:
    T& m_mutex;
};

struct BankAccount {
    BankAccount(int b) :Balance(b) {}
    int Balance;
    mutex Mutex;
};

void transformMoney(BankAccount& a, BankAccount& b, int money) {
    Lock<mutex> lockA(a.Mutex);
    Lock<mutex> lockB(b.Mutex);
    if(a.Balance <= money) return;
    a.Balance -= money;
    b.Balance += money;
}
/* BankAccount Alice;
 * BankAccount Bob;
 * transformMoney(Alice, Bob, 10);
 * transformMoney(Bob, Alice, 10);
 * 以上代码在多线程下可能死锁
 * */

void transformMoneyMuti(BankAccount& a, BankAccount& b, int money) {
    std::lock(a.Mutex, b.Mutex /* .... */);     // 对于传入的mutex全部锁住
    std::lock_guard<std::mutex> lockA(a.Mutex, std::adopt_lock); // 只调用析构函数释放锁
    std::lock_guard<std::mutex> lockB(b.Mutex, std::adopt_lock); // 只调用析构函数释放锁
    if(a.Balance <= money) return;
    a.Balance -= money;
    b.Balance += money;
}


int main() {

    return 0;
}
