//
// Created by 敬搏涛 on 2022/3/9.
//
#include "iostream"
#include <list>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <vector>



// 多线程案例 问题解决
using namespace std;

class Message {
public:
    const string& data() const {
        return m_data;
    }
    Message(string d = std::string()): m_data(move(d)) {}
private:
    string m_data;
};
std::list<Message> globalList;
std::atomic<bool> ready, quit;
std::mutex m_mutex;
//int totalSize = 0;
atomic<int> totalSize{0};
void worker() {
    while(!ready) {}
    while(!quit) {
        this_thread::sleep_for(std::chrono::milliseconds(1)); // 添加此语句降低cpu使用率
        Message msg;
        {
            lock_guard<mutex> lock(m_mutex);
            if(globalList.empty()) continue;
            auto iter = globalList.begin();
            // do something;
            // totalSize += strlen((*iter).data().c_str());
            msg = std::move(*iter);
            globalList.erase(iter);
        }
        totalSize += strlen(msg.data().c_str());
    }
}

int main() {
    auto nowc = clock(); // 计时器
    quit = false;
    ready = false;

    for(int i = 0; i < 10000; ++i) {
        globalList.push_back("this is a test" + to_string(i));
    }

    const auto threadCount = 4;
    vector<thread> pool;
    for(int i = 0; i < threadCount; ++i) {
        pool.emplace_back(worker);
    }
    ready = true;

    for(int i = 0; i < 10000; ++i) {
        std::this_thread::sleep_for(chrono::milliseconds(1));
        std::lock_guard<mutex> lock(m_mutex);
        globalList.push_back(string("second"));
    }

    while (true) {
        lock_guard<mutex> lock(m_mutex);
        if(globalList.empty()) {
            quit = true;
            break;
        }
    }

    for(auto &v: pool) {
        if(v.joinable()) {
            v.join();
        }
    }
    cout << "bye bye" << endl;
    auto finish = clock();
    cout << "总用时：" << finish - nowc << endl;
    cout << "总数据大小：" << totalSize << endl;
    return 1;
}

//总用时：34768390
//总用时：503426
//总耗时：169755

//总用时：527493
//总耗时：130150


//总耗时：155322
//总用时：22997158

//总用时：6436267
//总用时：522022

