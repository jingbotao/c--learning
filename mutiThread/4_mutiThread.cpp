//
// Created by 敬搏涛 on 2022/3/5.
//
#include <iostream>
#include <thread>
#include <future>
#include <cmath>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <atomic>
// 原子操作库
#include <mutex>
// 对操作进行加锁库
/* 1. 注意多次调用mutex的死锁问题
 * 2. 注意对与加锁未释放的问题
 * 3. 注意异常抛出mutex未释放的问题
 * */

using namespace  std;
// 多线程变量需要对共享变量进行处理
// 处理：
// 1. 取消变量共享，单独计算，然后汇总（如果没有必要的话，线程间不要共享资源）
class Counter {
public:
    void addCount() {
        m_count++;
    }
    int count() const {
        return m_count;
    }
    Counter(): m_count{0} {}
    void lockMutex() {
        m_mutex.lock();
    }
    void releaseMutex() {
        m_mutex.unlock();
    }
private:
    atomic<int> m_count; // 变量类型定义为原子整型，只能在同时一个进程操作该变量
    mutex m_mutex;
};

int work(int a) {
    return a + a;
}

template <class Iter>
void realWork(Counter &c, double &totalValue, Iter b, Iter e) {
    for(;b != e; ++b) {
        c.lockMutex();
        totalValue = totalValue + work(*b);
        c.releaseMutex();
        c.addCount();
    }
}


int main() {
    int n = std::thread::hardware_concurrency();
    std::cout << n << "个线程能够支持！" << std::endl;
    vector<int> vec;
    double totalValue;
    totalValue = 0;
    for(int i = 0; i < 10000000; ++i) {
        vec.push_back(rand() % 100);
    }
    Counter counter;
    realWork(counter, totalValue, vec.begin(), vec.end());
    std::cout << "total time: " << counter.count() << " " << totalValue << endl;
    totalValue = 0;
    Counter counter2;
    auto iter = vec.begin() + (vec.size() / 3);
    auto iter2 = vec.begin() + (vec.size() / 3 * 2);
    thread b([&counter2, &totalValue, iter, iter2] {
        realWork(counter2, totalValue, iter, iter2);
    });
    auto end = vec.end();
    thread c([&counter2, &totalValue, iter2, end] {
        realWork(counter2, totalValue, iter2, end);
    });
    realWork(counter2, totalValue, vec.begin(), iter);
    b.join();
    c.join();
    cout << "total times use multithread: " << counter2.count() << " " << totalValue << endl;
}
