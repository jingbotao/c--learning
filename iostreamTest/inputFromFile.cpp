//
// Created by 敬搏涛 on 2022/3/11.
//

#include "iostream"
#include "fstream"
#include "ostream"

using namespace std;

int main() {
//    ifstream data("/Users/jingbotao/CLionProjects/C++_demo/iostreamTest/training.txt");
    ifstream data("../iostreamTest/training.txt");
    ofstream outfile("../iostreamTest/out.txt", ios::app);
    if(!data.is_open()) {
        cout << "未打开文件!" << endl;
    }
    int i = 0;
    string temp;
    while(i++ < 10) {
        getline(data, temp);
        outfile << temp;
        outfile << endl;
    }

    return 1;
}