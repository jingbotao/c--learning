//
// Created by 敬搏涛 on 2022/3/11.
//
#include "iostream"
#include "fstream"
#include "ostream"
#include "string"
#include "cstring"
using namespace std;


string intToString(int v) {
    char buf[32] = {0};
    snprintf(buf, sizeof(buf), "%u", v);
    string str = buf;
    return str;
}


int main() {
    // ifstream data("/Users/jingbotao/CLionProjects/C++_demo/iostreamTest/training.txt");
    int a = 10;
    cout << "fisrt：" + intToString(a) << endl;
    return 1;
}
