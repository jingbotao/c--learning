//
// Created by 敬搏涛 on 2022/3/8.
//
#include <iostream>
#include <random>
using namespace std;
int main() {
    std::mt19937 gen(std::random_device{}());// 种子
    std::uniform_real_distribution<> distribution(0.0, 1.0);
    double result = distribution(gen);
    cout << result << endl;

    mt19937 mt_rand(time(0));
    cout << mt_rand() << endl;
    //shuffle(a+1, a+1+n, mt_rand);

    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)

    std::uniform_int_distribution<int> uni(0,10-1); // guaranteed unbiased
    auto random_integer = uni(rng);
    cout << random_integer << endl;
    return 0;
}
